'''
Created on Aug 14, 2018

@author: coditum
'''
name="Julia"
age=9
school="Elysian"
favSport="soccer"
birthday="October 6"
book="Harry Potter"
favCharacter="Hermione"
favFood="fish eggs with rice"
favClass="Math"
numOfSibling=1
favGame="Minecraft"
print(name)
print(age)
print(school)
print(favSport)
print(birthday)
print(book)
print(favCharacter)
print(favFood)
print(favClass)
print(numOfSibling)
print(favGame)
print("my name is "+name)
print("my age is "+str(age))
print("my school is "+school)
print("my favorite sport is "+favSport)
print("my birthday is "+birthday)
print("my favorite book is "+book)
print("my favorite character is "+favCharacter)
print("my favorite food is "+favFood)
print("my favorite Class is "+favClass)
print("I have "+str(numOfSibling)+" sibling.")
print("my favorite game is "+favGame)